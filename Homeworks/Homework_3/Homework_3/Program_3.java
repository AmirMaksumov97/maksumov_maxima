package Homework_3;
import java.util.Scanner;

public class Program_3 {
    public static double getDigitsAverage(int number){
        int sum = 0;
        double count =0;
        while (number != 0){
            sum += number%10;
            number = number/10;
            count++;
        }
        double result = sum / count;
        return result;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        double digitsAverage = getDigitsAverage(number);
        System.out.println(digitsAverage);
    }
}
