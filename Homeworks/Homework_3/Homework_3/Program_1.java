package Homework_3;
import java.util.Scanner;


public class Program_1 {
    public static int calculateMinDigit(int number){
        int result = 0;
        number = Math.abs(number);
        result = number % 10;
        while (number !=0){
            if(result > number % 10){
                result = number % 10;
            }
            number = number / 10;
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int currentNumber = scanner.nextInt();
        int minDigit = calculateMinDigit(currentNumber);
        System.out.print(minDigit);
    }
}
