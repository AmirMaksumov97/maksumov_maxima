package Homework_3;
import java.util.Scanner;

public class Program_4 {
    public static boolean isEven(int number) {

        if (number % 2 == 0) {
            return true;
        }
        return false;
    }

    public static double getDigitsAverage(int number) {
        double result = 0;
        int sum = 0;
        double count = 0;
        while (number != 0) {
            sum += number % 10;
            number = number / 10;
            count++;
        }
        result = sum / count;
        return result;
    }

    public static int calculateMinDigit(int number) {
        int result = 0;
        number = Math.abs(number);
        result = number % 10;
        while (number != 0) {
            if (result > number % 10) {
                result = number % 10;
            }
            number = number / 10;
        }
        return result;

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        while(number != -1) {
            boolean isEven = isEven(number);
            if (isEven) {
                int minDigit = calculateMinDigit(number);
                System.out.println(minDigit);
            } else {
                double digitsAverage = getDigitsAverage(number);
                System.out.println(digitsAverage);
            }
            number = scanner.nextInt();
        }
    }
}