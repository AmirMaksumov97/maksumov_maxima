import java.util.Scanner;

public class Program_2 {
    public static int findDuplicate(int[] array) {
        int[] numbers = new int[2000];
        int i;
        int result = 0;
        for (i = 0; i < array.length; i++) {
            int currentNumber = array[i];
            numbers[currentNumber]++;
        }
        int max = 0;
        for (i = 0; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }

        for (i = 0; i < numbers.length; i++) {
            if (numbers[i] == max) {
                result = i;
            }
        }
        return result;
    }

    public static int findDuplicateCount(int[] array) {
        int[] numbers = new int[2000];
        int i;
        int j = 0;
        for (i = 0; i < array.length; i++) {
            int currentNumber = array[i];
            numbers[currentNumber]++;
        }
        int max = 0;
        for (i = 0; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength;
        System.out.println("Введите колличество чисел");
        arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        System.out.println("Введите числа последовательности");
        int i;
        for (i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int j = findDuplicate(array);
        int k = findDuplicateCount(array);
        System.out.println("Число, встречающееся чаще остальных - " + j + " встречается " + k + " раз(а).");
    }
}
