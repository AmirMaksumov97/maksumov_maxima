import java.util.Scanner;

public class Program_1 {
    /*1. Реализовать функцию:

    pubic static int parse(int array[]);

    На вход функция принимает массив из целых положительных чисел, состоящих из одной цифры.
    Результатом выполнения данной функции является число, полученное на основе массива путем склеивания всех цифр.

    Например:

    int array[] = {4, 5, 7};

    int result = parse(array); // result == 457
    */
    public static int getParse(int array[]) {


        int number = 0;
        int i = 0;

        while (i < array.length) {
            number += (array[i] * ((int) Math.pow(10, (array.length - 1 - i))));
            i++;
        }
        return number;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];
        int i;
        for (i = 0; i < 5; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(getParse(array));
    }
}
