import java.util.Random;

public class RemoteController {
    private String model;
    private TV tv;
    public boolean isPlug;

    public RemoteController(String model) {
        this.model = model;
        isPlug = false;
    }

    public String getModel() {
        return model;
    }

    public void plugIn(TV tv) {
        if (tv.isOn) {
            this.tv = tv;
            isPlug = true;
            System.out.println("Вы подключились к телевизору " + this.tv.getName());
        } else {
            System.out.println("Сначала велючите телевизор!");
            isPlug = false;
        }
    }

    public void on(int channelNumber) {
        int r = new Random().nextInt(tv.channel.programs.length);
        System.out.println("Вы смотрите программу "+tv.channels[channelNumber].programs[r].getName()+" на канале " + tv.channels[channelNumber - 1].getName());

    }

    public void nextChannel() {

    }

    public void prevChannel() {

    }
}
