import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        TV sony = new TV("Sony");

        Channel tnt = new Channel("TNT");
        Channel ntv = new Channel("NTV");
        Channel mtv = new Channel("MTV");
        Channel rentv = new Channel("RenTV");


        Program program1 = new Program("Program1");
        Program program2 = new Program("Program2");
        Program program3 = new Program("Program3");
        Program program4 = new Program("Program4");
        Program program5 = new Program("Program5");


        sony.addChannel(tnt);
        sony.addChannel(ntv);
        sony.addChannel(mtv);
        sony.addChannel(rentv);



        sony.turnOnChannel(tnt);
        System.out.println(Arrays.toString(sony.channels));
        RemoteController rc1 = new RemoteController("rc1");
        sony.powerButton();
        rc1.plugIn(sony);
        mtv.addProgram(program1);
        mtv.addProgram(program2);
        mtv.addProgram(program3);
        mtv.addProgram(program4);
        mtv.addProgram(program5);
        rc1.on(3);
    }
}
