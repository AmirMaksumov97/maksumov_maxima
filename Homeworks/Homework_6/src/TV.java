public class TV {

    final public int Max_channels_count = 4;
    Channel[] channels = new Channel[Max_channels_count];
    int channelsCount = 0;
    public Channel channel;

    public void addChannel(Channel channel) {
        if (channelsCount < Max_channels_count) {
            channels[channelsCount] = channel;
            channelsCount++;
            System.out.println("Канал " + channel.getName() + " добавлен!");
        } else {
            System.err.println("Достигнуто максимальное кол-во каналов!");
        }
    }

    public void turnOnChannel(Channel channel) {
        this.channel = channel;
        System.out.println("Вы смотрите канал:" + channel.getName());
    }

    public TV(String name) {
        this.name = name;
        isOn = false; //Телевизор по умолчанию выключен//
    }

    private String name;
    public boolean isOn;


    public String getName() {
        return name;
    }

    public void powerButton() {
        this.isOn = !isOn;
        if (isOn) {
            System.out.println("Телевизор " + this.getName() + " включен");
        } else {
            System.out.println("Телевизор " + this.getName() + " выключен");

        }
    }


}
