public class Channel {
    public Channel(String name) {
        this.name = name;
    }

    private String name;
    final public int Max_programs_count = 4;
    Program[] programs = new Program[Max_programs_count];
    int programsCount = 0;
    private TV tv;
    private Program program;

    public void addProgram(Program program) {
        if (programsCount < Max_programs_count) {
            programs[programsCount] = program;
            programsCount++;
            System.out.println("Программа " + program.getName() + " добавлена на канал " + this.getName());
        } else {
            System.err.println("Достигнуто максимальное кол-во програм!");
        }
    }

    public String getName() {
        return name;
    }

}
