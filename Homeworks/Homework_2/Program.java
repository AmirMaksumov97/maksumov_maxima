

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] a = new int[10];
        int i, j = 0;
        for (i = 0; i < 10; i++)
            a[i] = scanner.nextInt();
        int[] b = new int[10];
        for (i = 0; i < 10; i++)
            b[i] = 0;
        for (i = 0; i < 10; i++)
            while (a[i] != 0) {
                b[i] = b[i] + a[i] % 10;
                a[i] = a[i] / 10;
            }
        for (i = 0; i < 10; i++)
            if (Math.abs(b[i]) < 18) j++;
        System.out.println(j);
    }
}
