public class ArrayList {
    public int[] elements;
    private final int INITIAL_SIZE = 10;

    public int getSize() {
        return size;
    }

    private int size;

    public ArrayList() {
        this.elements = new int[INITIAL_SIZE];
        this.size = 0;
    }

    public void add(int element) {
        if (size == elements.length) {
            int[] newElements = new int[elements.length + elements.length / 2];
            for (int i = 0; i < elements.length; i++) {
                newElements[i] = elements[i];
            }
            this.elements = newElements;
        }
        elements[size] = element;
        size++;
    }


    public int get(int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        }
        System.err.println("Index out of bounds");
        return -1;
    }
    public void addToBegin(int element){
        if (size == elements.length) {
            int[] newElements = new int[elements.length + elements.length / 2];
            for (int i = 0; i < elements.length; i++) {
                newElements[i] = elements[i];
            }
            this.elements = newElements;
        }
        shift();
        elements[0] = element;
        size++;
    }

    public int indexOf(int element) {
        int index = -1;
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                index = i;
                break;
            }
        }
        return index;
    }

    public int lastIndexOf(int element) {
        int index = -1;
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                index = i;
            }
        }
        return index;
    }

    public void remove(int index) {
        if (index >= 0 && index < size) {
            elements[index] = 0;
            size--;
            int temp;
            for (int i = index; i < elements.length - 1; i++) {
                temp = elements[i + 1];
                elements[i + 1] = elements[i];
                elements[i] = temp;
            }
        } else
            System.err.println("Index out of bounds");
    }

    public void removeAll(int element) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                elements[i] = 0;
                size--;
            }
        }
        int i = 0;

        for (int j = 0; j < elements.length; j++)
            if (elements[j] != 0)
                elements[i++] = elements[j];

        while (i < elements.length)
            elements[i++] = 0;
    }
    public void shift(){
        int temp;
        for (int i = elements.length-2; i > -1; i--) {
            temp = elements[i + 1];
            elements[i + 1] = elements[i];
            elements[i] = temp;
        }
    }
}
